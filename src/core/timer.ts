import {onBeforeUnmount, onMounted, ref} from "vue";

export const useTimer = (count: number, callback: (() => void)) => {
    const seconds = ref<number>(count);
    let timerId: ReturnType<typeof setInterval> | null = null;

    onMounted((): void => {
        doTimer();
    })

    onBeforeUnmount((): void => {
        if (timerId) {
            clearInterval(timerId);
            timerId = null;
        }
    });

    const doTimer = (): void => {
        timerId = setInterval((): void => {
            if (!seconds.value && timerId)
                clearInterval(timerId);
            if (seconds.value >= 1) seconds.value--;
            else callback();
        }, 1000);
    };

    return {seconds}
}
