import {createApp} from 'vue'
import App from '@/app.vue'
import router from '@/router'
import '@/assets/less/index.less';
import 'virtual:svg-icons-register'
import {createPinia} from 'pinia';

createApp(App)
    .use(createPinia())
    .use(router)
    .mount('#app');
