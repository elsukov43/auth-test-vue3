import {createRouter, createWebHistory, RouteLocationNormalized, RouteRecordRaw} from 'vue-router'
import {useUserStore} from '@/store/user';

declare module 'vue-router' {
    interface RouteMeta {
        isModal?: boolean;
        requiresAuth?: boolean;
    }
}

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'welcome',
        component: () => import('@/views/welcome.vue'),
        children: [
            {
                path: '/login',
                name: 'login',
                component: () => import('@/views/login.vue'),
                meta: {
                    isModal: true
                }
            },
            {
                path: '/success',
                name: 'success',
                component: () => import('@/views/success.vue'),
                meta: {
                    isModal: true,
                    requiresAuth: true
                }
            }
        ]
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to: RouteLocationNormalized) => {
    const store = useUserStore();
    if (to.meta.requiresAuth && !store.isAuthenticated)
        return '/login';
});

export default router