import {defineStore} from "pinia";

export interface User {
    login: string;
}

export interface PayloadSignIn {
    login: string;
    password: string;
}

interface UserState {
    user: User | null;
}

async function apiSignInEntry(payload: PayloadSignIn): Promise<User> {
    return new Promise<User>(resolve =>
        setTimeout(() => resolve(<User>{login: payload.login}), 1500)
    );
}

async function apiSignOutEntry(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
        return setTimeout((): void => {
            resolve(true);
        }, 1500)
    });
}

export const useUserStore = defineStore("userStore", {
    state: (): UserState => ({
        user: null
    }),
    actions: {
        async signInEntry(payload: PayloadSignIn): Promise<void> {
            try {
                this.user = await apiSignInEntry(payload);
            } catch (ex) {
                this.user = null;
                throw ex;
            }
        },
        async signOutEntry(): Promise<void> {
            const success = await apiSignOutEntry();
            if (success)
                this.user = null;
        }
    },
    getters: {
        isAuthenticated: (state: UserState): boolean => {
            return !!state.user;
        },
        getLogin: (state: UserState): string => {
            return state.user?.login ?? "Unknown";
        },
    }
})