import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import {resolve} from 'path'
import {createSvgIconsPlugin} from 'vite-plugin-svg-icons'

export default defineConfig({
    server: {
        host: "0.0.0.0"
    },
    resolve: {
        alias: {
            '@': resolve(__dirname, './src')
        }
    },
    build: {
        rollupOptions: {
            output: {
                manualChunks: {
                    'views': [
                        './src/views/welcome.vue',
                        './src/views/login.vue',
                        './src/views/success.vue',
                    ],
                },
            },
        },
    },
    plugins: [
        createSvgIconsPlugin({
            iconDirs: [resolve(__dirname, './src/assets/svg')],
            symbolId: '[name]',
        }),
        vue()]
})